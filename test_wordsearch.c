/*
 * Nitin Kumar
 * Section 1
 * 2/16/2015
 * E-mail: nkumar14@jhu.edu
 * 248-797-2533
 * JHED: nkumar14
 */

#include "wordsearch.h"
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
bool fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (!lhs || !rhs) return false;

    bool match = true;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

/*
 * Test fileeq on same file, files with same contents, files with
 * different contents and a file that doesn't exist
 */
void test_fileeq() {
    FILE* fptr = fopen("test1.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    fptr = fopen("test2.txt", "w");
    fprintf(fptr, "this\nis\na different test\n");
    fclose(fptr);

    fptr = fopen("test3.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    assert(fileeq("test1.txt", "test1.txt"));
    assert(fileeq("test2.txt", "test2.txt"));
    assert(!fileeq("test2.txt", "test1.txt"));
    assert(!fileeq("test1.txt", "test2.txt"));
    assert(fileeq("test3.txt", "test3.txt"));
    assert(fileeq("test1.txt", "test3.txt"));
    assert(fileeq("test3.txt", "test1.txt"));
    assert(!fileeq("test2.txt", "test3.txt"));
    assert(!fileeq("test3.txt", "test2.txt"));
    assert(!fileeq("", ""));  // can't open file
}


/*
 * writes out a hard-coded grid to a file;
 * uses read_grid to read in the grid;
 * test that the loaded grid is correct
 */
void test_read_grid() {
	
	char control_array[MAXSZ][MAXSZ] = {
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'}  
	};

	char test_array[MAXSZ][MAXSZ] = {{0}};
	
	FILE* fptr = fopen("test1.txt", "w");
	for (int i = 0; i < 5; i++){
		fprintf(fptr,"abcde\n");
	}
	fprintf(fptr,"\n");
	fclose(fptr);

	fptr = fopen("test1.txt", "r");
	int test_grid_num_rows = load_grid(fptr, test_array);

	assert(test_grid_num_rows == 5);
	for (int i = 0; i < MAXSZ; i++) {
		for (int j = 0; j < MAXSZ; j++) {
			assert(control_array[i][j] == test_array[i][j]);
		}
	}	
	
	fclose(fptr);


  //TODO
}

void test_find_init(char grid[][MAXSZ]) {
	
	char word[3] = {'b','a','\0'};	//make test word
	int coord[25][2] = {{0}}; 
	int test_ret[25][2] = {
		{0, 1} ,
		{1, 1} ,
		{2, 1} ,
		{3, 1} ,
		{4, 1} 
	}; 
	int test_find_letter = find_init_letter(grid, 5, word[0], coord); 
	assert(test_find_letter == 5);
	for(int i = 0; i < 5; i++) {
		for(int j = 0; j < 2; j++) {
			assert(coord[i][j] == test_ret[i][j]);
		}
	}
}
	

void test_find_forward(char grid[][MAXSZ]) {  //TODO 

	//Basic test success case with 2 letter word and given coordinates
	char word[3] = {'a','b','\0'};	//make left word

	for (int i = 0; i < 4; i++) {
		bool test_find_word = find_word_right(grid, 5, word, i, 0);
		assert(test_find_word == 1);
	}

}
void test_find_backward(char grid[][MAXSZ]) { //TODO 

	//Basic test success case with 2 letter word and given coordinates
	char word[3] = {'b','a','\0'};	//make up and down word

	for (int i = 4; i > 0; i--) {
		bool test_find_word = find_word_left(grid, word, i, 1);
		assert(test_find_word == 1);
	}

}
void test_find_up(char grid[][MAXSZ]) { 
	
	//Basic test success case with 2 letter word and given coordinates
	char word[3] = {'a','a','\0'};	//make up and down word

	for (int i = 0; i < 4; i++) {
		bool test_find_word = find_word_up(grid, 5, word, i, 0);
		assert(test_find_word == 1);
	}
	
	
  //TODO 

}
void test_find_down(char grid[][MAXSZ]) { //TODO 

	//Basic test success case with 2 letter word and given coordinates
	char word[3] = {'a','a','\0'};	//make up and down word

	for (int i = 4; i > 0; i--) {
		bool test_find_word = find_word_down(grid, word, i, 0);
		assert(test_find_word == 1);
	}

}

//Run all find tests in here off of same arrays
void test_find() {

	char control_array[MAXSZ][MAXSZ] = {	//init array
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'}  
	};

	test_find_init(control_array);
	test_find_backward(control_array);
	test_find_forward(control_array);
	test_find_up(control_array);
	test_find_down(control_array);
}

void test_write_all_matches() { //TODO 

	char control_array[MAXSZ][MAXSZ] = {	//init array
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'} , 
		{'a', 'b', 'c', 'd', 'e'}  
	};

	char word[] = "ab";
	FILE* outfile = fopen("test4.txt", "w");
	write_all_matches(outfile, control_array, 5, word);	
	fclose(outfile);

	word[0] = 'a';
	word[1] = 'c';
	outfile = fopen("test5.txt", "w");
	write_all_matches(outfile, control_array, 5, word);	
	fclose(outfile);
	
}


int main(void) {
    printf("Testing fileeq...\n");
    test_fileeq();
    printf("Passed fileeq tests.\n");

    printf("Running wordsearch tests...\n");
    test_read_grid();
    test_find();
    test_write_all_matches();
    printf("Passed wordsearch tests.\n");

}

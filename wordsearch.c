/*
 * Nitin Kumar
 * Section 1
 * 2/16/2015
 * E-mail: nkumar14@jhu.edu
 * 248-797-2533
 * JHED: nkumar14
 */

#include "wordsearch.h"
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

// NOTE: all the functions declared in wordsearch.h should be
// implemented in this file.

// reads square grid from infile until first empty line
// returns the number of rows in the grid or 0 for an error
// invalid if:
// 1. differing # of characters in rows
// 2. number of rows != number of columns
// 3. number of rows/columns = 0 ||  < 10
int load_grid(FILE* infile, char outgrid[][MAXSZ]){

	char cursor = fgetc(infile);

	if (cursor == (NULL || '\n')) {	//check if rows = 0
		printf("No rows in file or line break at beginning.\n");
		return 0;
	}

	int col_iterator = 0;
	int row_iterator = 1;
	
	for (col_iterator = 0; cursor != '\n'; col_iterator++) {
		if (col_iterator > MAXSZ) {		//check if columns > 10
			printf("Too many columns\n");
			return 0;
		}
		outgrid[0][col_iterator] = cursor;
		cursor = fgetc(infile);
	}

	int sec_iterator = 1;	//col_iterator for all lines after initial
	
	for (row_iterator = 1; sec_iterator != 0; row_iterator++) {//row_iterator is counter for iterating through rows

		if (row_iterator > col_iterator) {
			printf("More rows than columns\n");
			return 0;	//check if row amounts are bigger than columns
		}

		sec_iterator = 0;	//col_iterator for all lines after initial
		cursor = fgetc(infile);

		for (sec_iterator = 0; cursor != '\n'; sec_iterator++) {
			outgrid[row_iterator][sec_iterator] = cursor;
			cursor = fgetc(infile);
		}

		if(row_iterator == col_iterator && sec_iterator != col_iterator + 1) {
		}
		else if (sec_iterator != col_iterator) {
			printf("Different number of characters in each row or too few rows\n");
			return 0;//check if characters in row are different amounts
		}
		
	}

	if (row_iterator - 1 != col_iterator) {
		printf("Too few rows\n");
		return 0;	//check if row amounts are different than columns
	}

	return (row_iterator - 1);
}


// find first letter of word in grid
// use position for find_word function
// coord = [i] [j]
// w = first letter in word
// n = nxn size of grid
int find_init_letter(char grid[][MAXSZ], int n, char l, int coord[][2]) {
	int x = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (grid[i][j] == l) {
				coord[x][0] = i;
				coord[x][1] = j; 
				x++;
			}
		}
	}
	return x;
}

// return true if the word 'w' is in
// the n x n grid starting at the given i, j position
// and moving in the specified direction;
// n specifies the length and width of the grid (try
// to avoid code duplication in these; you may want
// other helper functions that they can all use)
bool find_word_down(char grid[][MAXSZ], int n, char w[], int i, int j) {
	if (grid[i][j] != w[0] || (n - i) < (int) strlen(w)) {	//safety checks the first letter of the word and that there are enough letters between edge of grid and initial letter
		return false;
	}

	int word_iterator = 0;	//iterator for letters in word
	
	while(grid[i][j] == w[word_iterator]) {
		word_iterator++;
		i++;
		if (i == n) {
			break;
		}
	}

	if((int) strlen(w) != word_iterator) {
		return false;
	}
	
	return true;
	
}
	
		
bool find_word_up(char grid[][MAXSZ], char w[], int i, int j) {
	if (grid[i][j] != w[0] || (i+1) < (int) strlen(w)) {	//safety checks the first letter of the word and that there are enough letters between edge of grid and initial letter
		return false;
	}

	int word_iterator = 0;	//iterator for letters in word
	
	while(grid[i][j] == w[word_iterator]) {
		word_iterator++;
		i--;
		if (i < 0) {
			break;
		}
	}

	if((int) strlen(w) != word_iterator) {
		return false;
	}
	
	return true;
}

bool find_word_left(char grid[][MAXSZ], char w[], int i, int j) {
	if (grid[i][j] != w[0] || (j+1) < (int) strlen(w)) {	//safety checks the first letter of the word and that there are enough letters between edge of grid and initial letter
		return false;
	}

	int word_iterator = 0;	//iterator for letters in word
	
	while(grid[i][j] == w[word_iterator]) {
		word_iterator++;
		j--;
		if (j < 0) {
			break;
		}
	}

	if((int) strlen(w) != word_iterator) {
		return false;
	}
	
	return true;
}

bool find_word_right(char grid[][MAXSZ], int n, char w[], int i, int j) {
	if (grid[i][j] != w[0] || (n - j) < (int) strlen(w)) {	//safety checks the first letter of the word and that there are enough letters between edge of grid and initial letter
		return false;
	}

	int word_iterator = 0;	//iterator for letters in word
	
	while(grid[i][j] == w[word_iterator]) {
		word_iterator++;
		j++;
		if (j == n) {
			break;
		}
	}

	if((int) strlen(w) != word_iterator) {
		return false;
	}
	
	return true;
}
// searches the n x n grid for the given word, w, and prints
// the matches to outfile; n specifies the length and width
// of the grid (which is square)
void write_all_matches(FILE* outfile, char grid[][MAXSZ], int n, char w[]) {
	int coord[n*n][2];		//array to hold point coordinates of all the potential first letters
	int poten_words = find_init_letter(grid, n, w[0], coord);	//finds all init letters and sets number of them to poten_words
	bool word_found = false;
	for (int c = 0; c < poten_words; c++) {
		int i = coord[c][0];
		int j = coord[c][1];
		if(find_word_down(grid, n, w, i, j)) {
			fprintf(outfile, "%s %d %d D\n", w, i, j);
			word_found = true;
		}
		if(find_word_up(grid, w, i, j)) {
			fprintf(outfile, "%s %d %d U\n", w, i, j);
			word_found = true;
		}
		if(find_word_right(grid, n, w, i, j)) {
			fprintf(outfile, "%s %d %d R\n", w, i, j);
			word_found = true;
		}
		if(find_word_left(grid, w, i, j)) {
			fprintf(outfile, "%s %d %d L\n", w, i, j);
			word_found = true;
		}
	}

	if(word_found == false) {
		fprintf(outfile, "%s- Not found\n", w);
	}
}	

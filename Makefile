# Sample Makefile for HW2
#
# USAGE:
#
# // to compile:
# make
#
# // to compile tests and run tests:
# make test
#
# // remove compilation output files:
# make clean
#

# make variables let us avoid pasting these options in multiple places
CC = gcc 
CFLAGS = -std=c99 -Wall -Wextra -pedantic -O         # for final build
DBCFLAGS = -std=c99 -Wall -Wextra -pedantic -g   # for debugging

bin: hw2

test: test_wordsearch
	echo "Running tests..."
	./test_wordsearch
	echo "All Tests Passed."

wordsearch.o: wordsearch.c wordsearch.h
	$(CC) $(CFLAGS) -c wordsearch.c
# (short for) gcc -std=c99 -Wall -Wextra -pedantic -O -c wordsearch.c

test_wordsearch.o: test_wordsearch.c wordsearch.h
	$(CC) $(CFLAGS) -c test_wordsearch.c

hw2.o: hw2.c wordsearch.h
	$(CC) $(CFLAGS) -pedantic -O -c hw2.c

test_wordsearch: test_wordsearch.o wordsearch.o
	$(CC) $(CFLAGS) -O -o test_wordsearch test_wordsearch.o wordsearch.o

test_debug: test_wordsearch.c wordsearch.o
	$(CC) $(DBCFLAGS) -o test_wordsearch test_wordsearch.c wordsearch.c
	gdb test_wordsearch

hw2: hw2.o wordsearch.o
	$(CC) $(CFLAGS) -O -o hw2 hw2.o wordsearch.o

hw2_debug:
	$(CC) $(DBCFLAGS) -o hw2 hw2.c wordsearch.o
	gdb hw2 

clean:
	rm -f *.o test_wordsearch hw2

git:
	git push origin master


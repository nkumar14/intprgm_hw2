/*
 * Nitin Kumar
 * Section 1
 * 2/16/2015
 * E-mail: nkumar14@jhu.edu
 * 248-797-2533
 * JHED: nkumar14
 */

#include "wordsearch.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>


// NOTE: this file should not contain any functions other than main();
// other functions should go in wordsearch.c, with their prototypes in
// wordsearch.h

int main(int argc, char* argv[]) {
	

	if(argc == 1) {
	}
	else if (argc == 2) {
		char grid[MAXSZ][MAXSZ]; 
		for (int i = 0; i < MAXSZ; i++) {
			for(int j = 0; j < MAXSZ; j++) {
				grid[i][j] = '\0';
			}
		}
		FILE* grid_file = fopen(argv[1], "r");
		int grid_size = load_grid(grid_file, grid);
		fclose(grid_file);
		if(grid_size == 0) {
			return 0;
		}

		char word[MAXSZ + 1] = {'\0'};
		char c = getchar();
		bool word_too_long = false;
		
		FILE* outfile = fopen("outfile.txt", "w");
		while(c != '\n') {
			word_too_long = false;
			int word_iterator = 0;
			while ((c != ' ') && (c != '\n')) {
				if (word_iterator < MAXSZ) {
					word[word_iterator] = c;
					c = getchar();
					word_iterator++;
				}
				else {
					word_too_long = true;
					word_iterator = MAXSZ;
					c = getchar();
				}
			}
			word[word_iterator] = '\0';
			if(word_too_long) {
				printf("Word with: %s at beginning is too long to be used as an input\n", word);
			}
			else {
				write_all_matches(outfile, grid, grid_size, word);
			}

			if(c == '\n'){
			}
			else {
				c = getchar();
			}
				

		}

		fclose(outfile); 
		
	}
	else {
		printf("Invalid input.");
	}

  return 0;
}
